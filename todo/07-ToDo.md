# ToDo

## Machine Learning

* [Moo - Genetic Algorithm Library](https://hackage.haskell.org/package/moo)

* [HLearn - Homomorphic machine learning](https://github.com/mikeizbicki/HLearn)

## Modelling

* [M.M.T Chakravarty - Playing with Graphics and Animations in Haskell](https://www.youtube.com/watch?v=9dk7_GDNoc)
 
* [Phil Freeman - Embedded DSLs in Haskell](https://www.youtube.com/watch?v=8DdyWgRYEeI)

## Algebra

* [Daniel Brice - Automatic Differentiation in Haskell](https://www.youtube.com/watch?v=q1DUKEOUoxA)

* [A Categorical Theory of Patches](https://arxiv.org/abs/1311.3903)

## Haskell Offshoots

* [Eta Lang](http://eta-lang.org/)

* [Purescript](http://www.purescript.org/)

## Case Study: Shake

* [Shake](http://shakebuild.com/manual) - Haskell Replacement for Make

## Case Study: Turtle

* [Turtle](https://github.com/Gabriel439/Haskell-Turtle-Library) - Type Safe Bash Scripting

## Case Study: Parsec

* [Parsec](http://jakewheat.github.io/intro_to_parsing/) - Introduction to Monadic Parsing

## Case Study: Alga

* [Alga](https://github.com/snowleopard/alga)

## Analysis, Synthesis and Judgement

* [Proofs and Types - Jean-Yves Girard](http://www.paultaylor.eu/stable/prot.pdf)

## Abstraction Methods

* TODO: Partial Orders/Metric Spaces

## Dependent Types / Refinement Types

* [Idris - A Language with Dependent Types](http://www.idris-lang.org/)

* [Liquid Types](http://goto.ucsd.edu/~rjhala/liquid/liquid_types.pdf)

## Homotopy Type Theory

* [Lean Theorem Prover](https://github.com/leanprover/lean)

* [Homotopy Type Theory - The Book](https://homotopytypetheory.org/)
