import (builtins.fetchTarball {
  name = "nixos-unstable-2018-02-15";
  url = "https://github.com/nixos/nixpkgs/archive/507855e56cc35006835276b75af66d2b20d7bd91.tar.gz";
  sha256 = "0rnlxy82c6dpxqf50rcrxv8ham45rk8vm910fn9q45xx67gfbv4s";
})
