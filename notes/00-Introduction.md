# Introduction

Welcome.

These resources attempt to provide an introductory guide to Haskell that will
help you to get to grips with the language fundamentals and start building real
world applications. This is not a book; resources are only loosely arranged in
order of relative difficulty and codified into a general framework. Haskell is a
language where often the great insights are hidden off the main track, in gists,
reddit threads, or a good stack overflow comment, and so the material that
comprises this guide is a heterogeneous mix of video lectures, blog posts,
snippets, questions, and example repositories.

The primary aim of this guide is to provide you with the confidence and
competencies you will need to navigate the Haskell ecosystem independently. Some
of the material here may not make complete sense immediately, and that's fine.
The course is arranged so that you can take a break, try something different, or
use your own initiative to hunt down what you need elsewhere on the web.
Intuitions for the more powerful concepts and mechanisms take time to build and
solidify, and you will need to reinforce the concepts by revisiting them at
regular intervals.

Haskell utilises concepts from mathematics in order to provide you with a high
standard of theoretical rigor, safety and confidence in the software you
produce. While it is by no means essential to gain a deep understanding of
mathematics in order to use Haskell to accomplish real tasks, the benefits of
doing so are real, and it is encouraged. This guide appreciates that some people
prefer to concentrate on technical knowledge, and ignore the mathematics, and so
the content here is also divided into a framework consisting of three principle
components:

1. The nature of human cognition; mathematics, logic and proof.
1. Haskell language elements; syntax, semantics, and types.
1. Practical problem solving with Haskell; applications, libraries and using
   abstractions.

In general there are two hurdles people must overcome in order to use
mathematics effectively to solve programming problems. The first is learning how
to read technical papers, which involves gaining a wide mathematical vocabulary
and a lot of practice. The second is understanding the precise nature of the
relationship between mathematical truth, the human mind, and real world
phenomena. This guide tries to cover both, as well provide a coherent history of
important ideas in the development of the philosophy of mathematics, primarily
concerning logic, type theory and category theory.

The Haskell ecosystem is vast, but often the truly great abstractions are
noticed very quickly and become a staple ingredient of a good developer toolkit.
Trying to get to grips with a new codebase in many languages can be an arduous
task, where as in Haskell once you become firmly familiar with the core concepts
and abstractions you'll start to notice them pop up in a variety of problem
domains that are wildly different in character, leaving you free to concentrate
on representing well your ideas. We cover some of these problem domains using
the relevant abstractions to illustrate this.

Finally, a forewarning: some of this content is trivial, some is easy, some is
moderate, and some of it is extremely hard. Some of the individual links in this
guide may require several months of continued effort in and of themselves in
order to understand perfectly. When traversing this guide, ask yourself if it's
something that you really feel you need to understand well right now in order to
progress, or if it's something that is worth processing in the back of your mind
for a period of time. This course makes no assumptions as to what you may want
to accomplish with your Haskell expertise, the only advice it gives is that the
best way to learn is to explore those things you find most fun and interesting
that are around your skill level, and everything else will fall into place.

Now, let's get started.

