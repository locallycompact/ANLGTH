# Journeyman

This section aims to get you familiar with higher-kinded types and abstract
algebra. We introduce Categories, Functors and Monoids. A good assistant for
this section is the [Functors, Applicative Functors and
Monoids](http://learnyouahaskell.com/functors-applicative-functors-and-monoids)
page from Learn You A Haskell.

## Category Theory

* [Category Theory for
Programmers](https://www.youtube.com/playlist?list=PLbgaMIhjbmEnaH_LTkxLI7FMa2HsnawM_) - Bartosz Milewski

* [Category Theory for Scientists](http://math.mit.edu/~dspivak/CT4S.pdf) - David Spivak

* [Category Theory in Context](http://www.math.jhu.edu/~eriehl/context.pdf) - Emily Riehl

* [Physics, Topology, Logic and Computation - A Rosetta Stone](http://math.ucr.edu/home/baez/rosetta.pdf) - John Baez, Mike Stay

Category Theory offers an arsenal of useful concepts and techniques in Haskell,
as well as a highly descriptive set of constructs for creating algebraic
abstractions. Category Theory uses a small handful of ideas to identify the
algebraic characteristics of different types of mathematical objects and their
theories. We do this in terms of the objects of the theory (these could be sets,
types, groups, shapes, manifolds, probability spaces, etc) and the morphisms of
the theory - a collection of arrows between the objects of the theory that in
some sense characterize it. We stipulate that for each object X in the category,
there must be an identity morphism called $id_X$, and that morphisms must be
composable and associative, so for morphisms $f : X \rightarrow Y$ and $g : Y
\rightarrow Z$, the composition $g \circ f : X \rightarrow Z$ exists, and for
any three composable morphisms $f$, $g$ and $h$, we have that $(h \circ g) \circ
f = h \circ (g \circ f)$.

So far we have seen some small insights into the theory of types and the theory
of propositions in intuitionistic logic, and we noted several things that were
similar, that they both had products (conjunctions) and sums (disjunctions) for
example. Let's first consider as a category the collection of Haskell types and
demonstrate that this is indeed a category. Here Haskell types are the objects
of the category, and the morphisms of the category are all possible Haskell
functions between any two Haskell types. We certainly have an identity
function for each object, as we have this function from Prelude:

```{.haskell}
id :: a -> a
```

We can monomorphize this polymorphic function for each type in Haskell.

We also need to show that we can compose any two morphisms between objects in
the category of Haskell types. We know we can do this with the dot operator:

```{.haskell}
(.) :: (b -> c) -> (a -> b) -> (a -> c)
```

and we know that this composition is associative. This gives us an
exhaustive representation of the theory of Haskell's type system, which we
will refer to as the category `Hask`.

We can similarly construct the category of propositional sentences, called
`Prop`, by taking propositions as the objects of the category, and morphisms
between them as proofs transforming a proof of one proposition into a proof of
another proposition. We have identity morphisms since any proposition $A$ can be
deduced from itself, and we have composition since any deductions $f : A
\rightarrow B$ and $g : B \rightarrow C$ can be composed to deduce $C$ from $A$
($g \circ f : A \rightarrow C)$.

Now that we have a basic sketch of our category `Hask`, we can examine it
in more detail to bring out its structure.

## Bicartesian Closed Categories

The category `Hask` has two data types that have unique properties.
they are the `Unit` type (written `()`) and the `Void` type.

```{.haskell}
data () = ()

data Void
```

The unit type has only one value constructor and no type parameters, so it only
has one possible value. This means any function that targets this type can only
do one thing, return `()`, and so the type `()` has the **universal property**
that for all other types `a` there is one possible morphism $a \rightarrow ()$, namely
the one that maps all values of type `a` to the only value of type `()`. An
object in a category that only has one morphism going into it from each other
object in the category is called a **terminal object**.

The void type has no value constructors, so it has no values at all! There is
one unique polymorphic function that takes values from this type, and returns
a value of type 'a', and it is called the absurd function.

```{.haskell}
absurd :: Void -> a
```

This function is clearly absurd as `Void` has *no values*, so we can never
generate an input for this function. All functions from Void are equivalent
to this function, so for every other type `a` we have one possible morphism
`Void -> a`, the absurd function. An object in a category that only has
one morphism going from it to each other object in the category is called
an **initial object**.

**TODO: Products, Sums, Exponential Objects**

Categories that have initial objects, terminal objects, products, sums
and exponential objects are called **Bicartesian Closed Categories**.

## Functors

A **functor** in Category Theory is a mapping from one category to another
category.  We have to consider not only objects, but also morphisms. A functor
must map objects in C to object D, and morphisms between objects in C to
morphisms between objects in D, such that identity morphisms are preserved and
composition of morphisms is coherent. Formally, for a functor $F$, the following
must hold:

* $F$ takes each object $X$ in the category $C$ to an object $F(X)$ in the
  category $D$.
* $F$ takes each morphism $f :: X \rightarrow Y$ in the category $C$ to a morphism
  $F(f) :: F(X) \rightarrow F(Y)$ in the category $D$ such that the following
  laws hold, for all objects $X$, $Y$, and $Z$ in $C$ and for all morphisms $f : X
  \rightarrow Y$ and $g : Y \rightarrow Z$.

$$F(id_{x}) = id_{F(X)}$$

$$F(g \circ f) = F(g) \circ F(f)$$

**TODO: Diagram**

An **endofunctor** is a functor from a category $C$ to itself. This is the
notion we will equip ourselves with, as we only have one category available
to us to play with, the category `Hask` of Haskell types. Remember that
the notion of a functor in Haskell specifically refers to an endofunctor
on `Hask`. The crucial aspect from the above definition is this part:

$$F :: (X \rightarrow Y) \rightarrow F(X) \rightarrow F(Y)$$

That is, the primary way to consider a functor is as a function that acts
on a morphism $X \rightarrow Y$ to another morphism $F(X) \rightarrow F(Y)$.
This translates into the following type class.

```{.haskell}
class Functor f where
  fmap :: (a -> b) -> f a -> f b
```

Here the function `fmap` in the Haskell definition takes the place of the $F$ in
the mathematical definition when speaking about morphism transformations. The
little `f` in the haskell definition is a one-parameter data type that is an
instance of functor, whose particular implementation of fmap will determine how
an arbitrary function is transformed with respect to that functor. Let's fire up
ghci and seea some concrete examples.

The two easiest examples of functors are `[]` and `Maybe`, here is the definition
of both:

```{.haskell}
instance Functor [] where
  fmap = map
```

```{.haskell}
instance Functor Maybe where
  fmap f (Just x) = Just (f x)
  fmap f Nothing = Nothing
```

What fmap allows us to do is to take an arbitrary function `(a -> b)`, and
with these instances transform it into either a function `([a] -> [b])` or
`Maybe a -> Maybe b`, depending on the context. In the case of lists, fmap
is just map, and we know how map works to turn an arbitrary function into
a function that operates on lists:

```{.haskell}
let a = [1,2,3]
fmap (+1) a 
```

A good intuition for `Maybe` is to consider it as a list that contains *at most
one element*. When we fmap a function f through it, we simply apply f to the inner value, if it exists, otherwise we just return Nothing.

```{.haskell}
let x = Just 5
let y = Nothing
fmap (+1) x -- prints "Just 6"
fmap (+1) y -- prints "Nothing"
```

* [Derive Foldable + Traversable](http://lpaste.net/144147)

## Applicative Functors

* [IsPalindrome with Applicative Reader](https://www.quora.com/How-does-the-following-Haskell-code-work-isPalindrome-*-reverse)

## Monoids

* [Equational Reasoning at Scale](http://www.haskellforall.com/2014/07/equational-reasoning-at-scale.html)

* [Monoids: Theme and Variation](http://dept.cs.williams.edu/~byorgey/pub/monoid-pearl.pdf)

## Stuff to Watch

* [Finite Flights on an Infinite Plane](https://www.youtube.com/watch?v=b04t8lsxMKQ) - Gary Fixler

This is a long talk, but a good exposition of how to go about modelling a
problem and refactoring types as you go. You can get a lot out of this one
if you stick with it.

