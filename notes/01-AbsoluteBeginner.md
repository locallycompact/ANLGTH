# Absolute Beginner

This section aims to get you set up and properly motivated. We collect some
provisional resources, some enlightening talks, set up a suitable development
environment, build our first application and get to grips with basic ideas about
types.

The links here are handy to come back to often, as some contain expert content
as well as beginner content.

## The Haskell Tool Stack

* [The Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/)

This guide strongly recommends stack as a package manager and build tool. Stack
is well established and reliable and you should find instructions for your
platform in the link above. Once installed, it should be possible to set up a
simple application as so:

    stack new foo
    cd foo
    stack build
    stack exec -- foo-exe

This should print the word "someFunc". If it did, you're set up correctly.
Smiley face.

It's worth a cursory glance through the default project structure at this point
as you'll be seeing it a lot. There are several files in a default project
template. In the above setup, the foo.cabal file contains information about your
project, a project by default will produce one library, one executable that uses
that library, and one test suite; indicated by separate sections. Each section
indicates the folders containing the haskell source files used to produce that
binary, the name of that binary in the case of an executable (for example
"foo-exe"), as well as any package dependencies needed from the haskell package
repository, "hackage. The code for the library is in src/Lib.hs, and it exports
a single function: `someFunc`. The code for the application is in app/Main.hs,
and it uses the `someFunc` function from the library and assigns it to the
`main` function, which is the function executed when running the application
`foo-exe`.

To avoid confusion as a beginner, you'll probably want to do most of your work
in the application itself to minimise the amount of moving parts you need to
consider. Try changing the main function to do each of these things in turn.
You'll need to rerun `stack build` and `stack exec -- foo-exe` to see the
results for each one.

```{.haskell}
main = print "Hello World"

main = print (1 + 2)

main = do
  let x = 5
  print x
```

Bear in mind some of the instructional links and videos on this page predate
stack by several years, and so some of them may ask you to compile with
ghc directly. You have the option here.

## Supplementary Resources

* [Learning How To
Learn](https://www.youtube.com/watch?v=j0XmixCsWjs) - Ed Kmett

Ed Kmett's talk on iterative learning is a great introduction to professional
problem solving, should be considered an essential talk for novices and experts
alike. Take a break and watch this.

* [What I Wish I Knew When Learning Haskell](http://dev.stephendiehl.com/hask/) - Steven Diehl

Steven Diehl's famous GitBook "What I Wish I Knew When Learning Haskell" is a
great *reference* to keep with you as you progress through the material. A great
deal of it is expert content, but it contains some good basics material you can
use to orient yourself in addition to what's here, and dip into the expert
content as needed. 

* [A Guide to GHC's Extensions](https://limperg.de/ghc-extensions/)

This is another good reference resource detailing Haskell's array of language
extensions.  Language extensions are just as important as libraries, allowing
for additions to the language that help you out in useful ways. Get some way
through basic reading and writing of Haskell before getting too deep, but
do keep it with you.

## Full Courses

* [Learn You A Haskell](http://learnyouahaskell.com/) - Miran Lipovača

Learn You A Haskell is still a great book, but some of it may be out of date so
treat it with a bit of caution.

* [Parallel and Concurrent Haskell](https://www.youtube.com/playlist?list=PLbgaMIhjbmEm_51-HWv9BQUXcmHYtl4sw) - Bartosz Milewski

Bartosz Milewski's series on Parallel and Concurrent Haskell, despite the name,
is an excellent introduction to the basics and only really touches on
parallelism towards the end. It's possible to start from scratch here.

* [Basics of Haskell](https://www.schoolofhaskell.com/user/bartosz/basics-of-haskell) - Bartosz Milweski

Another gentle introduction to Haskell for beginners, also by Bartosz.

* [Penn University CIS 194 Introduction To Haskell (Fall 2016)](http://www.seas.upenn.edu/~cis194/fall16/)

This is a great free online course that attempts to introduce Haskell using an
interactive online interpreter called CodeWorld. CodeWorld provides a simple
mathematical model for geometric objects, animations and interactivity,
and is a popular choice for schools. Not everything you learn here will carry
over into the wider world of Haskell, but it contains some good material.

## Mini-Courses

* [Learn Haskell in One Hour](https://www.youtube.com/watch?v=02_H3LjqMr8) - Derek Banas

This video covers a huge amount of content in just 75 minutes, and none of it
too difficult for a new user.

## The Nature of Types

Programing languages today are generally divided into two major paradigms,
imperative languages and declarative languages and it's important to understand
what that means. The atomic sentence in an imperative language is the
**instruction**, it tells the computer what to *do* i.e;

* add 2 to 5 and store it as 'x'
* stack three balls of snow on top of each other, add twigs on each side of the
  middle one and a carrot and two pebbles on the front of the top one
* when the acclerator is pressed, increase the speed of the car by x every frame

The atomic sentence of declarative languages is the **expression**, it tells the
computer what something *is* i.e;

* x is the sum of 2 and 5
* a snowman is a stack of three balls of snow, with twigs for arms, a carrot for a nose and pebbles for eyes.
* the speed of the car is a function of the acceleration and the time for which it has been accelerating

Haskell is a declarative language. All statements in Haskell are expressions,
not instructions, although some expression can be made to look like
instructions. Some of these expressions declare types, for example the Snowman
in the above example could be considered a type, and any individual snowman can
be considered a *value* of the type `Snowman`. Types are a very effective idea
in programming precisely because human categorization systems are so incredibly
old and *immensely* sophisticated and powerful. Categorisation helps us
determine which berries will kill us if we eat them, what material to use to
build homes out of, and it doesn't stop there; even small organisms can
distinguish between light and dark and can identify a sodium molecule and decide
what to do about it. Categorization is perhaps the oldest form of cognitive
process and gives a staggering amount of information, not just about how things
appear to us but, and perhaps even more primordially, *how* they can be used and
what their effects are on us and on the world; and one of the main themes of the
philosophy of Haskell is to give priority to describing precisely just *what the
thing is* that we are talking about, with greater and greater specificity.

The key insight here is that evaluating an expression is itself a computation,
we sometimes say the result is **synthesized**. We come up with a value for a
computation by evaluating it, by in turn evaluating all of its subexpressions.
A **function** is an expression too, one that expressed a mechanism for taking a
set of input values of certain types and relating them by a transformation to an
output of another type. Here is a function called `f`, that takes an argument
x of type Int, and returns an Int, (noted by the type signature `Int -> Int`)

```{.haskell}
f :: Int -> Int
f x = x + 1
```

Once we have declared this function, then wherever we come across the symbols `f
x`, where x is an integer, we know this will be replaced with the function body
`x + 1`. If we try to use a value of the wrong type in a function, Haskell will
not compile it for us. We can use types to indicate to ourselves and to the
compiler exactly which values will make sense in which expressions, by
restricting what can be used as input to values of specific types. For that, we
need to understand how to make our own types, and practice using them in
functions.

## Here Be Dragons

Let's take a look at some simple ideas. Create a new project called dragons
with `stack new dragons`. The following code you can put in your
`app/Main.hs` file.

```{.haskell}
data Caterpillar = Caterpillar deriving Show

data Butterfly = Butterfly deriving Show

cocoon :: Caterpillar -> Butterfly
cocoon Caterpillar = Butterfly
```

You can then change your main function to look like this:

```{.haskell}
main :: IO ()
main = do
  let x = Caterpillar 
  print x
  print $ cocoon x
```

Don't have two main functions or you'll get an error. You can run this with

```{.bash}
stack build
stack exec -- dragons-exe
```

You should get the output
```{.bash}
Caterpillar
Butterfly
```

Let's disect this, we have two data declarations; Butterfly and Caterpillar, and
we have two function declarations; cocoon and main.

A data declaration declares a type with all of its potential values. In a data
declaration, a type constructor is the thing on the left hand side of the equals
sign. The data constructor(s) are the things on the right hand side of the
equals sign. You use type constructors where a type is expected, and you use
data constructors where a value is expected. We reason about types and values
differently and this distinction is important to make clear upfront. In this case,
the type Butterfly has one and only one value, also called 'Butterfly', and likewise
with Caterpillar. Later we will make types that have several values, for now they
are boring.

The `deriving Show` part will make sure that values of our type can be printed
to the terminal, use this on your declarations where you can.

`cocoon` is a function, it turns a value of the type caterpillar in to a value
of the type butterfly. Functions come in two parts, a type signature and an
implementation.  The first line is the type signature, it indicates that this is
a function from Caterpillars to Butterflys. The second line is the
implementation, it says that wherever you see the expression `cocoon Caterpillar`,
you may replace it with the value `Butterfly`, and wherever you see the value
Butterfly you may replace it with the value `cocoon Caterpillar`. This is called
**equational reasoning**.

We also say that the function 'cocoon' **acts** on the value Caterpillar to produce the value Butterfly.

The main function will print things out for us, we assign `x` the value
`Caterpillar`, and then print x to the console, then we evaluate `cocoon x`, and
print that to the terminal. Since x is equal to `Caterpillar`, and `cocoon
Caterpillar` is equal to `Butterfly`, we print the value `Butterfly`. The dollar sign
`$` means "evaluate everything on the right first, and then pass the result to the
function on the left hand side. We'll say more about this later. 

So far our types aren't very interesting due to them only having one value,
let's add some more complex types.

```{.haskell}
data Dragon = Dragon {
  name :: String,
  heads :: Int
} deriving Show
```

This type `Dragon` is called a **product type**. It has to be constructed using
the value constructor `Dragon` and supplied two additional values of specific
types. It requires a String for a name, and an Int to determine what number of
heads it has. We can make two dragons like so: (add the following to your `do`
block in the main function, remember - lines in the do block must have the
correct indentation).

```{.haskell}
  let a = Dragon { name = "Bob", heads = 2 }
  let b = Dragon { name = "Schmebulok", heads = 3 }
  print a
  print b
  print $ name a -- prints "Bob"
  print $ heads b -- prints 3
```

Here we have constructed a couple of Dragons, one named Bob with two heads, and
one named Schmebulok with three heads. We assign these algebraic variable names
`a` and `b`.  Because our Dragon derives 'Show', we can print the whole Dragon
value with all of its component values. The field accessors `name` and `heads`
define functions with which we can extract particular values from a Dragon and
print those out separately.

Let's give our Dragon type a little more information, by adding a new type Color,
and giving Dragons a color attribute. Add the Color type to your code:

```{.haskell}
data Color = Red | Green | Blue deriving Show
```

The Color type has a range of possible values - Red, Green and Blue. This is 
called a **sum type**, it indicates that any of those three values can be
considered a value of type `Color`. Lets modify the dragon type so that it is
defined with a color.

```{.haskell}
data Dragon = Dragon {
  name :: String,
  heads :: Int,
  color :: Color
} deriving Show
```

And let's change Bob and Schmebulok so that they have a color. We need to do this
for the application to succesfully compile.

```{.haskell}
  let a = Dragon { name = "Bob", heads = 2, color = Blue }
  let b = Dragon { name = "Schmebulok", heads = 3, color = Red }
```

Now the Dragons will fight each other. Let's make a function called `fight`
that will take two dragons and return the "winner", the dragon that has the
most heads.

```{.haskell}
fight :: Dragon -> Dragon -> Dragon
fight x y = if heads x > heads y then x else y
```

This type signature has two possible interpretations. The first is that
fight is a function that accepts two arguments (both Dragons) and returns
one of the Dragons. The second interpretation is that of a function that
accepts one argument and returns a function that accepts the second
argument and returns the final result. Both of these interpretations
are fine, and it's important to get a sense for both in context. For the
moment, think of it as a two argument function.

The implementation of this function will compare the number of heads of x and y,
and return the dragon that has the most heads. Calling this function with our
two dragons and printing the result:


```{.haskell}
  print $ fight a b
```

should print out Schmebulok as the winner of the fight.

You can get the code for this section [here](https://gitlab.com/locallycompact/HaskellDragons)

## Things You Can Build

To master a new technology you have to play with it, and this section is
dedicated just to playing around with installing haskell applications with
stack. If you're interested in continuing with the language proper, you can
skip this section. However if you're interested in understanding the scope of
what stack does, getting some practice in building other people's code, or are
having problems getting stack to work, then look here for some projects that are
fairly easy to build.

Stack is a build tool and a package manager, and is the preferred way to install
haskell based command line applications. Stack aims for reproducible builds by
building all of the dependencies of the application locally on your computer at
curated versions that are all known to work together, and this can be done on a
per project basis. It's fun to install stuff with stack, as you always know what
you're getting.

All applications installed with stack end up in ~/.local/bin, so you should add
this to your PATH.

* [hledger](http://hledger.org/)

hledger is a command line accounting tool that you can install by sitting in
your home directory and running `stack install hledger`. This
[video](https://www.youtube.com/watch?v=H_CdGzLbc7A) by the author of
hledger is mostly a runthrough of how the application works and is organised,
but also some tidbits on stack, testing, IDEs and the benefits of types. A very
lowkey talk that's here because there is not much in the way of code in it.

* [pandoc](http://pandoc.org/getting-started.html)

pandoc is one of the most famous haskell applications, dubbed the swiss
army knife of document conversion. It is also used to power the website
you're reading now. Again, you can install it by sitting in your home
directory and typing `stack install pandoc`.

* [hakyll](https://jaspervdj.be/hakyll/)

hakyll is a fun static site generator built and configured using haskell, and
powered by pandoc. You can use it to make simple websites and blogs. Sit in your
home directory and do this:

```
stack install hakyll
hakyll-init mysite
cd mysite
stack init
stack build
stack exec -- site server 
```

You should be able to see your website by navigating your browser to
localhost:8000. You can also generate the site normally by just running

```
stack exec -- site build
```

and opening the '_site/index.html' file in your browser.

* [a site like the one you're reading now](http://zenhaskell.gitlab.io/shakebook)

You can use pandoc and [shake](http://shakebuild.com/) to create a book like the
one you're reading now to take your own Haskell notes. Shake is a powerful build
tool that serves as a replacement for 'make' to build small or large projects.
Instead of Makefiles, shake uses Shakefiles that are written in Haskell. The
[Shake manual](http://shakebuild.com/manual) assumes no prior Haskell knowledge
to get started writing Shakefiles, but they can be fairly complicated. To get
started with the template, you can do this:

```
stack install pandoc shake
git clone https://gitlab.com/locallycompact/GitbookTemplate
cd GitbookTemplate
stack exec shake test
```
