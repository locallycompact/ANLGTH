# Fledgling

This section aims to broaden your core skillset before tackling the harder
concepts. We introduce lambda calculus, hoogle, higher-order functions and
loading and using YAML data.

## Lambda Calculus

* [The Lambda Calulus](https://plato.stanford.edu/entries/lambda-calculus/) - SEP

* [Introduction to Lambda Calculus](http://www.cse.chalmers.se/research/group/logic/TypesSS05/Extra/geuvers.pdf) - Henk Barendregt, Erik Barendsen

Lambda calculus is simply a *notation* for writing functions that apply to
arguments, and it is this that should give you an better intuition for why
multi-argument type signatures can look like they do, and still make sense.
Consider the `fight` function from the dragons repository:

```{.haskell}
fight :: Dragon -> Dragon -> Dragon
fight x y = if heads x > heads y then x else y
```

We mentioned that this can be though of in two ways: As a function that takes
two `Dragon`s and returns a `Dragon`, or as a function that takes one `Dragon`
and returns a function `Dragon -> Dragon`, i.e takes the first argument and
returns a function that accepts the second argument. For example, we could do
this.

```{.haskell}
let bob = Dragon { name = "Bob", color = Red, heads = 3}
let fightBob = fight bob
```

The function fightBob has the type `Dragon -> Dragon`. It's a function that
takes any Dragon as an argument and forces it to fight bob. We say that the
value of bob has been **substituted** into the function body. If were were to see
the implementation of this function body, it would look like this:

```{.haskell}
fightBob :: Dragon -> Dragon
fightBob y = if heads Dragon { name = "Bob", color = Red, heads = 3} > heads y then Dragon { name = "Bob", color = Red, heads = 3} else y
```

Haskell has figured this out for us, so we can move fightBob around as if it were
a function we had declared ourself. Function application is the strongest
operation in Haskell - it is always evaluated first. So if you see a line
like

```{.haskell}
x = f a <$> g b (++) h j l p
```

then you should be looking to try and determine what the types of the
subexpressions are *first* (what type is `f a`, `g b` and `h j l p`), which will
then give you some idea of how the expression fits together as a whole.

The lambda calculus abstraction itself does not concern itself with types per
se, it is simply a formal representation of how expressions can be substituted
into other expressions. The type system sits on top of the lambda calculus to
form a **typed lambda calculus**, whose job it is to prevent you from
substituting just any old expression into any other expression, as you might
expect. (i.e, You can't pass a Cat to fightBob, as that wouldn't be very fair.)

It's worth noting here that you can use lambda syntax directly to make anonymous
functions that are type safe. For example, you can put this in the interpreter.

```{.haskell}
(\x y -> 2*x + y) 2 3
```

You can also assign lambda functions to a variable

```{.haskell}
let f = \x y z -> z ++ y ++ x
f "foo" "bar" "baz"
```

Anonymous lambda expressions can be good in a pinch where you don't need to
reuse the function elsewhere in the code.

## Laziness

* [The Incomplete Guide to Lazy Evaluation](https://hackhands.com/guide-lazy-evaluation-haskell/) - Heinrich Apfelmus

* [School of Haskell - Laziness](https://www.schoolofhaskell.com/school/starting-with-haskell/introduction-to-haskell/6-laziness)

You may have seen Haskell referred to elsewhere as a **lazy** language. This
means that expressions that are substituted into a body of code are only ever
evaluated when they are actually needed to synthesize a result. For example,
suppose we generate an infinite list of integers from 1 to infinity like this
(press Ctrl+C to cancel the printing)

```{.haskell}
[1..]
```

In many other languages, you would never be able to write this expression
anywhere in your code without causing it to run indefinitely, but the Haskell
compiler can determine for us when we don't require it. Notice here that the z
argument in the lambda expression is unused.

```{.haskell}
 (\x y z -> x ++ y) [1..10] [11..20] [1..]
```

Or perhaps, we only want the first 10 elements of the infinite list


```{.haskell}
take 10 [1..]
```

Take a look at the links above for some more detailed examples of laziness in
action.

## Higher Order Functions

Higher order functions is the informal name given to a function that accepts
other functions as arguments. When designing programs, it's often best to try
and conceptualise a very small piece of functionality and implement it well.
Higher order functions serve as schemas to take small programs and use them to
craft bigger programs. Let's fire up ghci and look at a few examples.

Take a look at the type for the 'map' function.

```{.haskell}
:t map
map :: (a -> b) -> [a] -> [b]
```

map is a higher order function. Its first argument is a function `(a -> b)`. Its
second argument is a list `[a]`. The map function loops over each element of the
list `[a]`, turning them all into `b`s and giving us back a list `[b]`. Note that
this function is perfectly paramterized - the types 'a' and 'b' can be anything,
so long as they align with the type of the function being used to transform them.
Let's see a few examples, we can make a short list of numbers and map various
functions over the list:

```{.haskell}
let a = [1,2,3]
let f = \x -> x*2

map f a
map show a
map (/5) a
```

Take a look at the type for the 'zip' function

```{.haskell}
:t zip
zip :: [a] -> [b] -> [(a,b)]
```

Zip is not a higher order function, but a fairly simple one. zip takes two
arguments, both lists, but they can be lists of different types. It returns a
list of pairs, taking one element from one list and one element from the other,
like a zip. Give it a try

```{.haskell}
zip [1,2,3] ["a", "b", "c"]
```

Should return `[(1,"a"), (2,"b"), (3,"c")]`. Have a play with this function until
you're happy with it.

Now let's have a look at a slightly more complex function, the `zipWith` function.


```{.haskell}
:t zipWith
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
```

zipWith is a three argument function. It adds an extra argument onto the front
of zip, which is itself a function (a -> b -> c). What this will do is take
what would have been the resulting list of pairs [(a,b)] of zip, and run them
through a two argument function that transforms them into something else. Let's
try zipWith with a simple example where `a`, `b`, and `c` are all integers.

```{.haskell}
zipWith (+) [1,2,3] [2,4,5]
```

This should print out the list `[3,6,8]`.

We can take advantage of recursion, laziness and zipWith to make a concise
description of the fibonacci sequence. Have a look at the type of the `(:)`
(called 'cons') operator.

```{.haskell}
:t (:)
(:) :: a -> [a] -> [a]
```

(:) is an infix operator that takes a head element of type a, and a list of
a's as the tail, and returns a new list with the head element prepended.
You can try it like so:

```{.haskell}
1 : [2, 3]
```

We can use zipWith the cons operator too.

```{.haskell}
fib = 0 : 1 : zipWith fib (tail fib)

take 10 fib
```

See if you can convince yourself why this works.

## Using Hoogle

* [hoogle](https://www.haskell.org/hoogle/)

Hoogle is the type-signature search engine. You can use it to lookup type
signatures for function that you think might exist, but aren't sure what they're
called. For example, we might have a list [a], and a function (a -> b) and want
to make a list [b] out of it. We know this is possible, but have forgotten the name
of it. We can just use hoogle to find it for us by searching for the type
signature:

```{.haskell}
(a -> b) -> [a] -> [b]
```

and find out that this is just the map function. It doesn't matter which
way around we put the arguments either. We can just as easily search for

```{.haskell}
[a] -> (a -> b) -> [b]
```

and get the same results. We also get a bunch of other results that involve more
specific type class constraints.


## Working With YAML

YAML is fairly pleasant to work with in Haskell, as you can just represent the
schema of the YAML directly as a type in your code. Let's extend our dragons
example so we can load dragons in from YAML files. We'll have to add bytestring,
docopt and yaml to the dependencies in our cabal file.

```
executable dragons-exe
  hs-source-dirs:      app
  main-is:             Main.hs
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N
  build-depends:       base
                     , bytestring
                     , docopt
                     , yaml
  default-language:    Haskell2010
```

We're going to need to use the documentation for Data.Yaml, which you can find
here (search the page for the 'decode' function, which is the one we will be
using):

* [Data.Yaml](https://hackage.haskell.org/package/yaml-0.8.22/docs/src/Data-Yaml.html#decode)

Let's make a simple `USAGE.txt` file. We only need one command, and we're going
to use it to pit two dragons against each other.

```{.haskell}
Usage:
  dragons-exe fight <d1> <d2>
```

Add some things to your `app/Main.hs` so that it looks like this. We need a few
more language extensions, a few more imports, and we'll need to modify our
data types to derive some more type classes.

```{.haskell}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes #-}

module Main where

import Control.Monad (when)
import qualified Data.ByteString.Char8 as BS
import Data.Yaml
import GHC.Generics
import System.Environment (getArgs)
import System.Console.Docopt

data Color = Red | Green | Blue deriving (Show, Generic, FromJSON, ToJSON)

data Dragon = Dragon {
  name :: String,
  heads :: Int,
  color :: Color
} deriving (Show, Generic, FromJSON, ToJSON)

fight :: Dragon -> Dragon -> Dragon
fight x y = if heads x > heads y then x else y

patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns
```

The additions we've made to handle YAML are the two language extensions -
DeriveAnyClass and DeriveGeneric, and we've added a few additional type class
derivations that indicate both Color and Dragon can be both serialized to and
deserialized from yaml data (the ToJSON and FromJSON classes respectively). Make
a directory called 'data' and make two files one called 'data/bob.yml': 


```{.yaml}
name: Bob
heads: 3
color: Red
```

and one called 'data/shmebulock.yml':

```{.yaml}
name: Shmebulock
heads: 4
color: Blue
```

Let's add our main function, which for now will accept two dragon files,
deserialize, and print them. No fighting yet.

```{.haskell}
main :: IO ()
main = do
  args <- parseArgsOrExit patterns =<< getArgs

  when (args `isPresent` (command "fight")) $ do
    f1 <- BS.readFile =<< args `getArgOrExit` (argument "d1")
    f2 <- BS.readFile =<< args `getArgOrExit` (argument "d2")
    let d1 = decode f1 :: Maybe Dragon
    let d2 = decode f2 :: Maybe Dragon
    print d1
    print d2
```

Give it a build and try running

```{.bash}
stack exec -- dragons-exe fight bob.yml shmebulock.yml
```

Give yourself a small reward if that worked and let's take a look at the code.

You'll notice that the decode function has this type signature:

```{.haskell}
decode :: FromJSON a => ByteString -> Maybe a
```

Let's first look at the return type `Maybe a`. Maybe is an algebraic data
type that is defined like so:

```{.haskell}
data Maybe a = Just a | Nothing
```

That is, there are two ways of constructing a value of Maybe a, either by using
the `Just` value constructor, or the `Nothing` value constructor. We use the
Maybe type to represent the type of a computation that has the potential to
*fail*. In this case, the implementation of decode is taking a ByteString, and
deciding whether or not it can decode the ByteString into the specified type
`a`, if it can, decode will yield `Just a`, otherwise it will yield `Nothing`.
Haskell has no concept of 'null' like other languages might, and so the Maybe
type is an important data type for representing failing computations in a type
safe manner.

The first part (`FromJSON a`) is a constraint on the free type `a`. We are
transforming a ByteString into some type that can be decoded from YAML, and so a
must derive FromJSON. We have to explcitily annotate which type we're attempting
to decode to, as Haskell can't guess this for us. We do this by the extra
annotation `:: Maybe Dragon`, indicating that decode might give us a Dragon, or
might give us Nothing. When we print these Dragons out, they are wrapped in the
`Just` constructor.

This presents a puzzling problem when we try to fight these Dragons together.
Our `fight` function accepts two Dragons, but the values we have are of type
`Maybe Dragon`, so our function will not accept these directly. One option is to
attempt to unwrap them via the
[fromJust](https://downloads.haskell.org/~ghc/8.0.1/docs/html/libraries/base-4.9.0.0/Data-Maybe.html#v:fromJust)
in Data.Maybe, to turn a `Maybe Dragon` into a `Dragon`. This function will
throw a runtime error if the value of your `Maybe Dragon` is `Nothing`, but
perhaps this is the behaviour you want. See if you can print out the winner
of the fight using fromJust.

The other approach is to try and transform the function itself into a function
that accepts `Maybe Dragons` instead of Dragons. This solution is in the source
code for this section, that can be found
[here](https://gitlab.com/locallycompact/HaskellDragons2).

You'll need to do some investigating to make it make sense.

## Stuff to Try

* [A program to solve Sudoku - Richard Bird](http://www.cmi.ac.in/~spsuresh/teaching/prgh15/papers/sudoku.pdf)

A Sudoku solver that only requires things we've covered so far. You'll need
several hours and the [Data.List
documentation](https://hackage.haskell.org/package/base-4.9.1.0/docs/Data-List.html)
to help you.

