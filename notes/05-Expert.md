# Expert

## Monads

* ***TODO: makeDeck***

* [Notions of computation and Monads](https://core.ac.uk/download/pdf/21173011.pdf)

* [Three Useful Monads](http://adit.io/posts/2013-06-10-three-useful-monads.html)

* [Ten example uses of Monads](http://haskellexists.blogspot.co.uk/2017/02/ten-example-uses-of-monads.html)

## Comonads

* [Evaluating cellular automata is comonadic](http://blog.sigfpe.com/2006/12/evaluating-cellular-automata-is.html)

## Free Monads

* [Free for DSLs, cofree for interpreters](http://dlaing.org/cofun/posts/free_and_cofree.html)

