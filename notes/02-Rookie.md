# Rookie

This section aims to give you a few useful skills to play with. We introduce
some rudimentary ideas in logic, the notion of function composition,
writing short algorithms, and how to write command line applications.

## Intuitionistic Logic

* [Intuitionism in the Philosophy of
Mathematics](http://plato.stanford.edu/entries/intuitionism/) - SEP

The Stanford Encyclopedia of Philosophy has some great articles on the
philosophy of mathematical logic. This article is a good reference for the
history of Brouwer's Intuitionistic logic, originally formulated in the early
1900s that later evolved into the theoretical basis for Haskell's type system.

The upshot of Intuitionistic Logic is that it differs from Classical Logic by
deprecating truth in favour of proof - a statement can't be said to be true
without providing a proof for consideration, and deprecating falsity in favour
of refutability, - a statement can't be said to be false without a proof of its
refutation. For a proof `a` of a proposition `A` we write

$$a : A$$

to indicate that A has a proof. Notice this is very similar to denoting a value
of a particular type $a :: A$. Now if we have two propositions A, and B and want
to denote a proof of both of them, we can write this as a pair of proofs of the
conjunction, like so

$$(a,b) : A \land B$$
 
This is more or less similar to classical logic in that, the truth of a
conjunction depends on the truth of both A and B simultaneously, and in
intuitionistic logic we require both a proof a of A and a proof b of B
simultaneously.

Disjunction however, is slightly different. It is similar
in that in order to prove a proposition $A \lor B$, we need only supply a
proof for either A or B.

$$a_A : A \lor B\ \ or\ \ b_B : A \lor B$$

However, in that we must actively construct a proof, and anything we may later
derive from that proposition will require an extension of that proof, we must
carry knowledge of *which* proposition we are proving, which I have denoted with
the subscripts $a_A$ and $b_B$.  Consider a body of code in an imperative
language:

```{.python}
if (proposition1 or proposition2):
  dosomething() 
```

After evaluating the if condition, we lose all knowledge of which proposition it
was that passed us into the body. If we require this knowledge later, we will
need to recompute it. We can use Haskell to remedy situations like this
by keeping histories of the significance of computed values, which we will
explore in detail later, or you can check out these links:

* [Boolean Blindness](https://existentialtype.wordpress.com/2011/03/15/boolean-blindness/) - Robert Harper

* [Overcoming Boolean Blindness With Evidence](https://cs-syd.eu/posts/2016-07-24-overcoming-boolean-blindness-evidence.html) - Tom Sydney Kerckhove

The final notion is transformations of proofs, which corresponds to a
deductive implication. Suppose $a : A$ and we aim to prove a proposition $B$,
we can do this if we can find a transformation f which can transform any proof
$a : A$ into a proof $f(a) : B$, written as so:

$$f : A \rightarrow B$$

i.e, $f$ is a proof that we can deduce the proposition $B$ from $A$.

## Propositions as Types

* [Propositions as Types](http://homepages.inf.ed.ac.uk/wadler/papers/propositions-as-types/propositions-as-types.pdf) - Phil Wadler

The key insight here is that the mechanisms by which we reason about logic, i.e
conjunctions, disjunctions, and transformations of proofs, are algebraically
identical to products, sums and functions of types. This isomorphism is
known as the [Curry-Howard Correspondence](https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence).

* [Type Theory Foundations](https://www.youtube.com/playlist?list=PLGCr8P_YncjXRzdGq2SjKv5F2J8HUFeqN) - Robert Harper

This short series on Type Theory is a great way to bring home the ideas of
intuitionistic logic and get a feel for the underlying algebra.

## Using the interpreter

As well as using stack to make projects, you should become familiar with
using the interpreter. Change directory to one of your stack projects
and run

```{.bash}
stack exec ghci
```

You should now find yourself in the interpreter. Stack will bring in
any library dependencies you need, so you can experiment here. Let's
define a function (we can define it without the type signature):

```{.haskell}
f x = x ++ "bar"
```

We can check the type of this function 'f' by running:

```{.bash}
:t f
```
and we see that it has the type

```{.haskell}
f :: [Char] -> [Char]
```

which is equivalent to

```{.haskell}
f :: x ++ "bar"
```

because a String is just a list of characters in Haskell. So we can apply
our function to a String like so

```{.haskell}
f "foo"
```

which will print out the string "foobar"


## Composition

Function composition is the heart of functional programming, allowing us
to connect the output of one function to the input of another function. Let's
compose two functions, our function f from above, and the function `reverse`
from the Data.List module. We can import this in the interpreter by running

```{.haskell}
import Data.List
```

We can then see the type of reverse by running

```{.haskell}
:t reverse
```

this function has the type

```{.haskell}
reverse :: [a] -> [a]
```

This means that reverse takes a list of values of type a, and also returns a
list of values of type a. The specific type of the elements of the list is
unimportant, we can reverse a list of any type! Let's try running it on a few
lists

```{.haskell}
reverse [1,2,3]

reverse "Hello World"
```

Composition allows us to connect two functions if the output of one function
has the same type as the input of the next function, and this can be done with
 the dot operator like so:

```{.haskell}
let h = reverse . f
```

Check the type of h here:

```{.haskell}
:t h

h :: [Char] -> [Char]
```

For two functions f and g, the function `g . f` reads "g after f" or, "run f
on the input, then run g on the output of f", but we've simplified it into
a single function. Our function `h` will first run `f`, which adds "bar"
to the input string, and then it will reverse the whole thing.

Now we can run

```{.haskell}
h "foo"
```

which should print out the string "raboof". We can make our programs simpler
by liberal use of composition.

## Pattern Matching and Destructuring

Haskell functions take arguments of a certain type, but how do we handle cases
in which the argument takes a range of values that should lead to different
forms of result? A typical programming language might rely on liberal if else
statements or switch or case statements, we can do a little better in haskell with
the use of pattern matching. Consider the following program. You will need
to use the `:{` and `:}` to enter a multi line function into your interpreter.

```{.haskell}
:{
qsort [] = []
qsort (x:xs) = qsort [y | y <- xs, y < x] ++ [x] ++ qsort [y | y <- xs, y >= x]
:}
```

Our function qsort takes a list and attempts to sort it, but does so with two
separate implementations. We have one pattern for the empty list, sorting an
empty list is just again an empty list. The other pattern is for when the list
contains at least one element. This notation **destructures** the list into two
parts, a head element called `x`, and the rest of the list, called the `xs`
(read "the 'x'es"). 

The qsort here is a good example of a function in Haskell that represents the
underlying algorithm directly. You can think of the algorithm like so:

1. Take the head of the list (the 'x')
1. Put it in smack in the middle
1. Find all the things that are lesser than x, put them to the left
1. Find all the things that are greater than x, put them to the right
1. Sort both sides using the same approach

You can give it a go in the interpreter by applying it to a list.

```{.haskell}
qsort [5, 4, 2, 3, 1]

qsort "Hello world"
```

We can take a look at the type of qsort in the interpreter.

```{.haskell}
:t qsort 
qsort :: Ord t => [t] -> [t]
```

qsort takes a list and returns a list of the same type, but this type signature also
has a **constraint** in it. The `Ord t =>` part indicates that the values that make
up the list must be *orderable*, that is they must be the kinds of things that can
be compared with 'less than' and 'greater than' operators. Haskell has picked up
this information by the fact that we have used those operators in the implementation,
and so would prevent us from trying to use the qsort function on lists of elements
that can't be compared for inequality. So, we would not be able to sort a list
of Dragons using this function, unless we implement the Ord type class and the
associating '<' and '>' operators for the Dragon type. We'll look at how this works
in a later section.

Here are a few links to supplement this section

* [Introduction to Haskell](https://wiki.haskell.org/Introduction) - Haskell
Wiki, contains a few variant representations of the qsort function, including
this one.

* [List comprehensions](https://wiki.haskell.org/List_comprehension) - Haskell wiki, some list comprehension examples.

## Building Command Line Applications

Making CLI applications is important enough that it should go here, at the risk
of getting ahead of ourselves. We're going to try and put together a DocOpt
application so that we can get used to the approach, and disect the code a
little. We'll need more tricks to understand the code completely though, so
don't worry about it. Here's a link to the DocOpt documentation:

* [DocOpt](https://github.com/docopt/docopt.hs)  - Document Driven Command Line Interface

Make a new stack project called `stack new myprog`. You'll need to edit the
.cabal file and add the dependency `docopt` to the `build-depends` in the
executable file. That section should look something like this:

```
executable myprog-exe
  hs-source-dirs:      app
  main-is:             Main.hs
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N
  build-depends:       base
                     , myprog
                     , docopt
  default-language:    Haskell2010
```

Run a `stack build` here and stack should download and build docopt for you.

Make a USAGE.txt file as described and put it in the root of the application,
next to the cabal file.

```
Usage:
  myprog cat <file>
  myprog echo [--caps] <string>

Options:
  -c, --caps    Caps-lock the echoed argument
```

The guide says to put the following code in a file called MyProg.hs. Don't do that. Instead just
replace the code in your app/Main.hs file.

```{.haskell}
{-# LANGUAGE QuasiQuotes #-}
import Control.Monad (when)
import Data.Char (toUpper)
import System.Environment (getArgs)
import System.Console.Docopt

patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns

main = do
  args <- parseArgsOrExit patterns =<< getArgs

  when (args `isPresent` (command "cat")) $ do
    file <- args `getArgOrExit` (argument "file")
    putStr =<< readFile file

  when (args `isPresent` (command "echo")) $ do
    let charTransform = if args `isPresent` (longOption "caps")
                          then toUpper
                          else id
    string <- args `getArgOrExit` (argument "string")
    putStrLn $ map charTransform string
```

Now run

```
stack build
stack exec -- myprog
```

And you should be greeted with the command line usage instructions. We can
also try to use the actual commands.

```
stack exec -- myprog echo "foo"
stack exec -- myprog echo "foo" --caps
stack exec -- myprog cat app/Main.hs
```

There's a lot to take in here. Let's break it down.

```{.haskell}
{-# LANGUAGE QuasiQuotes #-}
```

This line indicates a **language extension**. A great deal of the power in
Haskell comes not necessarily from libraries, but from extending the language
directly to be able to do new things. A common mistake among new users is to
avoid using language extensions as if they are something for experts. Don't do
this, you'll need them if you're going to get anywhere.

The language extension `QuasiQuotes` allows us to pull in non-haskell code
and compile it secretly to Haskell. In this case, DocOpt is taking advantage
of QuasiQuotes in order to bring the USAGE.txt file in and run it through
the docoptFile function, which is a function from `System.Console.Docopt`.

```{.haskell}
patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]
```

The square brackets here, are a syntax that is only available with QuasiQuotes
enabled.

You may have noticed this word **monad** crop up and wondered what the hell it
means. [Don't](http://dev.stephendiehl.com/hask/#eightfold-path-to-monad-satori)
go [looking this
up](https://byorgey.wordpress.com/2009/01/12/abstraction-intuition-and-the-monad-tutorial-fallacy/)
now. We're in this for the long haul, the aim here is to introduce concepts at
the right time and burn Haskell into your mind slowly. For now, take a look at
the form of the code. `when` more or less looks and works like an `if`
statement, the code:

```{.haskell}
when b $ do
  f
```

will execute the function f when the boolean value b is True.

You can find the code for this section [here](https://gitlab.com/locallycompact/DocoptTemplate)

