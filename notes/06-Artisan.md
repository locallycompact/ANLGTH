# Artisan

## Arrows

* [Arrows are Strong Monads](http://www-kb.is.s.u-tokyo.ac.jp/~asada/papers/arrStrMnd.pdf)

## Profunctors

* [I Love Profunctors, They're So Easy](https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/profunctors)

* [Profunctors in Haskell](http://blog.sigfpe.com/2011/07/profunctors-in-haskell.html)

* [George Wilson - The Extended Functor Family](https://www.youtube.com/watch?v=JZPXzJ5tp9w)

* [Phil Freeman - Fun With Profunctors](https://www.youtube.com/watch?v=OJtGECfksds)

* [Addressing Pieces of State With Profunctors](http://blog.sigfpe.com/2017/01/addressing-pieces-of-state-with.html)

* [Profunctors - nCatLab](https://ncatlab.org/nlab/show/profunctor)

## Optics

* [A Little Lens Starter Tutorial](https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/a-little-lens-starter-tutorial)

* [Tony Morris - Let's Lens](https://www.youtube.com/playlist?list=PLFTBfi-r3xj39pyV0uHO0G-u8UNnXbmg0)

* [Productization of Functional Optics](Brian McKenna - Productionisation of Functional Optics)

* [Edward Kmett - Monad Transformer Lenses](https://www.youtube.com/watch?v=Bxcz23GOJqc)

