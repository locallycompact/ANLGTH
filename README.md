# A Nonlinear Guide To Haskell

My collection of Haskell notes, I hope you find it useful.

## Building Locally

You can build with either nix or stack. Nix is preferred, 

### Building With Nix

Drop into a reproducible build environment with

    nix-shell

If you have [cachix](https://cachix.org/), you can speed this up by running

    cachix use locallycompact

You can then run `shake` from within the nix shell.

    shake

You can test the ouput in chromium by running

    shake test
D
You can build the pdf and slide copies with

    shake pdf
    shake beamer

Clean up with

    shake clean

### Building With Stack

You will need stack.

    stack build

You will also need to install dihaa as it is the only dependency not picked up
as a library.

    stack install dihaa

This will create the site in the folder `public`. This will take a while the
first time you run it.

You can then test this in chromium by doing

    stack exec -- site test

You can build the pdf and slide copies with

    stack exec -- site pdf
    stack exec -- site beamer

or change the browser by editing the Shakefile.hs.

You can clean up with

    stack exec -- site clean

If anything goes haywire, just `git clean -dfx` and rebuild from scratch.
